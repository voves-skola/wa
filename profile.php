<?php
require "./include/general.php";
require "./include/auth_guard.php";
require "./include/database.php";

$error = "";

// Pokud je formulář odeslaný jako POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $email = mysqli_real_escape_string($conn, $_POST["email"]);
    $result = mysqli_query($conn, "SELECT COUNT(1) FROM `users` WHERE `email` = '$email'");

    $firstname = mysqli_real_escape_string($conn, $_POST["firstname"]);
    $lastname = mysqli_real_escape_string($conn, $_POST["lastname"]);
    $phone = mysqli_real_escape_string($conn, $_POST["phone"]);

    mysqli_query($conn, "UPDATE `users` SET `firstname` = '$firstname', `lastname` = '$lastname', `phone` = '$phone' WHERE `email` = '{$_SESSION['email']}'");

    $_SESSION["firstname"] = $firstname;
    $_SESSION["lastname"] = $lastname;
    $_SESSION["phone"] = $phone;
}
?>

<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Upravit profil</h1>
                    </header>

                    <hr class="major" />

                    <form method="post">
                        <div class="row gtr-uniform">
                            <div class="col-6 col-12-xsmall">
                                <p>Kontaktní údaje</p>
                                <input type="text" name="firstname" id="first-name" placeholder="Křestní jméno" required value="<?php echo $_SESSION["firstname"] ?>" /><br>
                                <input type="text" name="lastname" id="last-name" placeholder="Příjmení" required value="<?php echo $_SESSION["lastname"] ?>" /><br>
                                <input type="email" name="email" id="email" placeholder="E-mail" required value="<?php echo $_SESSION["email"] ?>" disabled /><br>
                                <input type="tel" name="phone" id="phone" placeholder="Telefonní číslo" pattern="\+\d{12}" required value="<?php echo $_SESSION["phone"] ?>" /><br>

                                <p id="message" style="color: red;"><?php echo $error ?></p>
                            </div>

                            <div class="col-12">
                                <button type="submit" class="button primary">
                                    Odeslat
                                </button>
                            </div>
                        </div>
                    </form>
                </section>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>
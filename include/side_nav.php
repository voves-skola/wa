<!-- Sidebar -->
<div id="sidebar">
    <div class="inner">

        <!-- Menu -->
        <nav id="menu">
            <header class="major">
                <h2>Menu</h2>
            </header>
            <ul>
                <li><a href="/ondra-wa/index.php">Úvodní stránka</a></li>

                <li>
                    <span class="opener">Uživatel</span>
                    <?php if (isset($_SESSION["email"])) : ?>
                        <ul>
                            <li><a href="/ondra-wa/profile.php">Profil</a></li>
                            <li><a href="/ondra-wa/logout.php">Odhlásit se</a></li>
                            <li><a href="/ondra-wa/delete_user.php" onclick="return confirm('Opravdu si chceš smazat účet?')">Vymazat účet</a></li>
                            <li><a href="/ondra-wa/user_articles.php">Moje příspěvky</a></li>
                        </ul>
                    <?php else : ?>
                        <ul>
                            <li> <a href="/ondra-wa/login.php">Přihlásit se</a></li>
                            <li><a href="/ondra-wa/registration.php">Zaregistrovat se</a></li>
                        </ul>
                    <?php endif; ?>
                </li>



                <?php if ($_SESSION["admin"] ?? false) : ?>

                    <li>
                        <span class="opener">Admin</span>
                        <ul>
                            <li> <a href="/ondra-wa/user_list.php">Seznam uživatelů</a></li>
                        </ul>
                    </li>


                <?php endif; ?>

                <li><a href="/ondra-wa/history.php">Historie počítačů</a></li>

                <li>
                    <span class="opener">Počítačové komponenty</span>
                    <ul>
                        <li> <a href="/ondra-wa/components/procesors.php">Procesory</a></li>
                        <li><a href="/ondra-wa/components/graphic_cards.php">Grafiky</a></li>
                        <li> <a href="/ondra-wa/components/motherboard.php">Základní desky</a></li>
                        <li> <a href="/ondra-wa/components/power_supply.php">Zdroje</a></li>
                        <li><a href="/ondra-wa/components/cases.php">Počítačové skříně</a></li>
                        <li> <a href="/ondra-wa/components/ram.php">Paměť RAM</a></li>
                        <li> <a href="/ondra-wa/components/disks.php">Disky a SSD</a></li>
                    </ul>
                </li>

                <li>
                    <span class="opener">Články</span>
                    <ul>
                        <li> <a href="/ondra-wa/articles.php">Seznam článků</a></li>
                        <li> <a href="/ondra-wa/form.php">Nový článek</a></li>
                    </ul>
                </li>
                <li> <a href="/ondra-wa/sources.php">Odkazy na weby</a></li>
            </ul>
        </nav>

        <!-- Footer -->
        <footer id="footer">
            <p class="copyright">&copy; Untitled. All rights reserved. Demo Images: <a href="https://unsplash.com">Unsplash</a>. Design: <a href="https://html5up.net">HTML5
                    UP</a>.</p>
        </footer>

    </div>
</div>
<?php
require "./include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Historie Počítačů</h1>
                        <p>Moderní společnost si bez počítačů nelze vůbec představit.
                            Považujeme je za absolutní samozřejmost, přitom před sto lety vůbec neexistovaly.
                            Kdy se vlastně objevil historicky první počítač na světě? Podle čeho se liší jednotlivé generace?
                            A kdo stál za tím, že počítače vůbec vznikly a zasáhly tak významnou měrou do našich životů?</p>

                        <img src="obrazky\pocitac-ibm-pc.jpg" alt="IBM počítač">
                        <ol>
                            <li><a href="#dejiny">Dějiny počítačů</a> </li>
                            <li><a href="#nultaGenerace">Nultá generace počítačů</a></li>
                            <li><a href="#prvniGenerace">První generace počítačů</a></li>
                            <li><a href="#druhaGenerace">Druhá generace počítačů</a></li>
                            <li><a href="#tretiGenerace">Třetí generace počítačů</a></li>
                            <li><a href="#ctvrtaGenerace">Čtvrtá generace počítačů</a></li>
                            <li><a href="#applePocitac">První Apple počítač</a></li>
                            <li><a href="#budoucnost">Výhledy do budoucnosti počítačů</a></li>
                        </ol>
                    </header>

                    <hr class="major" />


                    <!-- Dějiny pc -->
                    <h2 id="dejiny">Dějiny počítačů</h2>
                    <p>Při pohledu do historie musíme uznat, že lidstvo je mimořádně vynalézavé.
                        Už totiž přibližně před 5 000 lety se v Babylonii používal tzv. Abakus.
                        Šlo o zaprášený kámen, který usnadnil počítání s čísly.
                        Ve starověkém Řecku a Římě se používala dřevěná nebo hliněná destička,
                        do které se vkládaly kamínky „calculli“ – odtud název kalkulačka.
                        A někdy mezi roky 150 a 100 př. n. l. se v okolí řeckého ostrova Antikythera používal mechanický
                        kalkulátor,
                        který podle dnešních poznatků sloužil k výpočtu kalendáře, resp. polohy Slunce, Měsíce a dalších
                        planet.</p>
                    <p>Pokud se hodně posuneme v čase,
                        tak v roce 1614 objevil John Napier novou matematickou metodu umožňující realizovat násobení a
                        dělení pomocí
                        sčítání a odčítání s využitím logaritmů.
                        Díky tomu se sestavily první logaritmické tabulky a následně logaritmické pravítko, které se dokonce
                        používalo
                        i k provádění výpočtů v rámci programu Apollo.</p>
                    <p>Celá historie počítačů je poměrně košatá a zejména od půlky 20. století to začíná být hodně zajímavé.
                        I proto se přistoupilo k rozdělení na jednotlivé generace.
                        Pojem počítačová generace však nemá zcela jasně vymezené časové hranice,
                        z čehož vyplývá celá řada nejasností ohledně příslušnosti některých počítačů k dané generaci a
                        stanovení
                        průběhu generací samotných.
                        Pojďme se na ně přesto podívat.</p>
                    <img src="obrazky\historie-pc-pascalina-1.jpg" alt="pascalina">
                    <p>Naše menší ohlédnutí do daleké historie zakončíme představením analogového počítače.
                        Například na konci 19. století byl zkonstruován 10komponentní stroj pro předpovídání mořského
                        přílivu a
                        odlivu.
                        Tyto stroje se široce používaly při konstrukci oficiálních přílivových předpovědí pro obecnou
                        námořní
                        navigaci.
                        Ve 20. století pak byly vojensky důležité během první světové války a znovu během druhé světové
                        války,
                        kdy vylepšený americký stroj sloužil k předpovědi přílivu a odlivu pro den vylodění v Normandii.</p>
                    <img src="obrazky\analogovy-pocitac.jpg" alt="Analogovy-pocitac">
                    <p>Analogové počítače byly široce používány ve vědeckých a průmyslových aplikacích i po nástupu
                        digitálních
                        počítačů, protože v té době byly obvykle rychlejší.
                        Své využití si však udržely v některých konkrétních aplikacích, jako jsou letadlové letové
                        simulátory,
                        letový počítač v letadle i pro výuku řídicích systémů na vysokých školách.</p>

                    <!-- Nultá generace počítačů -->
                    <h2 id="nultaGenerace">Nultá generace počítačů</h2>
                    <p>Nacházíme se na pomezí 30. a 40. let minulého století,
                        kdy německý inženýr Konrad Zuse dokončil základní návrh stroje Z1 pracujícího ve dvojkové soustavě s
                        aritmetikou v plovoucí čárce.
                        Rychle následovaly počítače Z2 i Z3. Mimochodem, Z3 byl užíván též k výpočtům charakteristik
                        balistických
                        raket V-2.
                        Už tady vidíme, jak důležité historické události stimulují technologický pokrok (později například
                        již zmíněný
                        vesmírný program Apollo).</p>
                    <p>V podobném duchu pracoval i tým kolem Howarda Hathaway Aikena.
                        Tento americký počítačový průkopník působil jako hlavní inženýr při stavbě jednoho z prvních
                        počítačů firmy
                        IBM:
                        Harvard Mark I. Počítač byl dodán v roce 1944 na harvardskou univerzitu, kde zabíral prostory o
                        velikosti
                        zhruba patnáct metrů.
                        Uplatňoval se zejména pro vědeckotechnické výpočty a býval většinou umístěný na vědeckých nebo
                        univerzitních
                        pracovištích.
                        Jak to tak bývá, svůj zájem projevila i armáda, u které rychlá práce s čísly sloužila k výpočtu
                        balistických
                        tabulek.</p>
                    <h3>První počítač v Československu – SAPO</h3>
                    <p>Počátky výpočetní techniky v bývalém Československu jsou spojeny s pracovištěm,
                        které po většinu své existence neslo název Výzkumný ústav matematických strojů.
                        Založili jej lidé, kteří se během druhé světové války a těsně po ní dostali v USA do styku s tehdy
                        vznikajícími prvními počítači.
                        Nejvýznačnější osobností byl profesor Antonín Svoboda.</p>
                    <p>A právě on stál na počátku zrodu počítače SAPO (SAmočinný POčítač).
                        SAPO byl reléový počítač s magnetickou bubnovou pamětí o kapacitě 1 024 slov po 32 dvojkových
                        číslicích,
                        kompletně sestavený v roce 1958.
                        Pracoval ve dvojkové soustavě s pohyblivou řádovou čárkou.
                        Vstup byl dvojkový nebo dekadický, děrnoštítkový.
                        Celý stroj obsahoval 7 000 relé a 400 elektronek a fyzicky zabíral hned několik místností.
                        Jeho zajímavostí bylo, že obsahoval tři identické procesory, které samostatně počítaly zadané úlohy.
                        O správném výsledku pak rozhodovalo hlasování – když byl shodný alespoň ve dvou případech, byl
                        považován za
                        správný.
                        Pokud se ve všech třech případech lišil, operace se opakovala.</p>
                    <img src="obrazky\prvni-pocitac-sapo.jpg" alt="sapo-pocitac">
                    <p>Tři roky po svém spuštění, tedy v roce 1960, však počítač SAPO shořel.
                        Z jiskřících reléových kontaktů se totiž vzňala loužička oleje, kterým se relé promazávala.
                        Celkově je nutné brát na zřetel časový posun, který znamenal, že rozvoj počítačů v Československu
                        byl o
                        pěkných pár let opožděn ve srovnání například s USA.</p>

                    <!-- První generace počítačů -->
                    <h2 id="prvniGenerace">První generace počítačů</h2>
                    <P>Na pomezí 40. a 50. let se rozvíjela první generace počítačů, která byla charakteristická použitím
                        elektronek.
                        Elektronka je zařízení usměrňující nebo zesilující elektrické signály. Díky tomu bylo možné
                        odstranit pomalé a
                        nespolehlivé mechanické relé.
                        Tyto počítače jsou vybudovány podle von Neumannova schématu, proto je pro ně charakteristický
                        diskrétní režim
                        práce.
                        To znamená, že do paměti počítače je zaveden vždy jeden program a data, se kterými se aktuálně
                        pracuje.
                        Poté je spuštěn výpočet, v jehož průběhu již není možné s počítačem nijak pracovat.
                        Po skončení výpočtu musí operátor do počítače zavést další program a nová data.</P>
                    <P>Typickou ukázkou první počítačové generace je ENIAC (Electronic Numerical Integrator And Calculator)
                        postavený
                        na Pensylvánské státní univerzitě.
                        Zajímavostí je fakt, že chlazení zajišťovaly dva letecké motory.
                        ENIAC byl původně navržen pro složité matematické výpočty palebných tabulek pro dělostřelectvo
                        americké
                        armády.
                        Nicméně druhá světová válka skončila dříve, než mohl být počítač plně nasazen.
                        Později své kvality ukázal při vývoji termonukleárních zbraní.</P>
                    <img src="obrazky\historie-pc-epos-1.jpg" alt="epos pocitac">
                    <P>Pochopitelně nesmíme vynechat ani československou větev, jejíž nedílnou součástí je EPOS 1
                        (Elektronický
                        POčítací Stroj).
                        Opět vznikl v týmu docenta Svobody, a dokonce existoval ve třech exemplářích.
                        Podle původní koncepce měl mít počítač spuštěný v roce 1960 hned 2 000 elektronek.
                        Po Svobodově emigraci byl však počítač sestaven z 3 400 elektronek a měl příkon 80 kW.
                        Konečný vzor, který byl později uveden do průmyslu, měl 8 000 elektronek a příkon 200 až 300 kW.</P>

                    <!-- Druhá generace počítačů-->
                    <h2 id="druhaGenerace">Druhá generace počítačů</h2>
                    <p>Posouváme se do období mezi lety 1957 až 1965, do kterého se všeobecně řadí druhá počítačová generace.
                        A přesně v tento moment začíná lidstvo poprvé využívat tranzistory.
                        Jde o polovodičovou součástku, s jejíž významnou pomocí se podařilo počítače nejen fyzicky zmenšit,
                        ale také
                        citelně zlepšit jejich výkon i spolehlivost.
                        A mimochodem, tranzistory jsou dodnes základem všech integrovaných obvodů.</p>
                    <p>UNIVAC I (UNIVersal Automatic Computer I) byl vůbec první sériově vyráběný počítač původem z USA.
                        Původně byl navržen pro obchod a administrativní použití. Původní cena UNIVACu I byla 159 000 USD,
                        avšak cena
                        postupem času rostla,
                        až se vyšplhala na rozmezí mezi 1 250 000 USD až 1 500 000 USD. Celkem bylo vyrobeno a dodáno 46
                        strojů.
                        Pro většinu univerzit tak byl počítač z pohledu financí nedostupný. U soukromých společností však
                        sloužil
                        velmi dobře.
                        Například v té době významná firma Sperry Rand používala dva tyto systémy v New Yorku až do roku
                        1968.</p>
                    <img src="obrazky\historie-pc-univac-1.jpg" alt="pocitac-univac">
                    <p>V našich končinách se podařilo vylepšit počítač EPOS na jeho druhou generaci.
                        Samozřejmě s již plným využitím moderních tranzistorů.
                        Počítač EPOS 2 byl od roku 1969 vyráběn v ZPA Čakovice pod označením ZPA 600, od roku 1972 pak
                        prošel
                        modernizací na ZPA 601.
                        Obou verzí bylo vyrobeno více než 30 kusů. Původní sálové počítače se tak konečně podařilo nahradit
                        mobilnější
                        verzí, kterou si například nechala armáda nainstalovat do vozů se skříňovou nástavbou.</p>

                    <!-- Třetí generace pořítačů -->
                    <h2 id="tretiGenerace">Třetí generace počítačů</h2>
                    <p>Ve třetí generaci, která je časově ohraničena lety 1965 až 1980, se setkáváme s integrovanými obvody
                        (IO).
                        Jde o elektronickou součástku složenou z mnoha jednoduchých součástek, které společně tvoří
                        elektrický obvod.
                        Ruku v ruce s tím strmě roste počet tranzistorů i celkový výkon.
                        Taktéž se snad vůbec poprvé objevuje pojem proces, kdy CPU zpracovává jednu úlohu (proces), ale
                        zároveň
                        pracuje s daty jiné úlohy.
                        Počítače třetí generace jsou stále označované jako sálové PC, ale postupně došlo i na skutečné
                        mikropočítače,
                        které šlo snadno přenášet z místa na místo.</p>
                    <p>Mezi ikonické představitele třetí generace jistě patří IBM System 360.
                        Tento počítač, který byl firmou IBM nabízen od roku 1965, měl procesorovou jednotku složenou z
                        diskrétních
                        elektronických součástek, tj. především tranzistorů,
                        diod a rezistorů. Snadno rozšiřitelná modulární platforma si našla mnoho způsobů využití a stojí za
                        vzestupem
                        IBM.
                        Přestože se dostaly počítače IBM do velkých firem i institucí po celém světě,
                        nejslavnějším uplatněním stále zůstává nasazení počítačů System 360 při přistání člověka na Měsíci.
                        Pět superpočítačů IBM System 360 (Model 75) monitorovalo v reálném čase všechny informace
                        přicházející z
                        vesmírné lodi Apollo 11.</p>
                    <img src="obrazky\historie-pc-ibm-360.jpg" alt="ibm-pocitac">
                    <p>Pokud se podíváme na situaci v Československu jako na součást socialistického bloku, klesala možnost
                        dovozu
                        potřebných součástek ze západního světa.
                        Vzorem pro řadu počítačů JSEP-R1 byl právě americký počítač IBM System 360.
                        Dostupná technologická základna však nedovolovala přímé kopírování, a počítače se musely navrhovat z
                        domácích
                        součástek.
                        Sériová výroba některých systémů řady JSEP-R1, označovaných původně jako RJAD 1, byla pak zahájena v
                        roce
                        1972, což je poměrně velký časový skluz vůči originálním PC v USA.
                        Československo se v první fázi programu JSEP podílelo na vývoji jednoho počítače
                        a téměř třiceti druhů přídavných zařízení včetně zařízení pro přípravu dat a příslušného
                        programového
                        vybavení.</p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/ljkMl5G00Gw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

                    <!-- Čtvrtá generace počítačů -->
                    <h2 id="ctvrtaGenerace">Čtvrtá generace počítačů</h2>
                    <p>Celé období od roku 1981 se počítá do čtvrté počítačové generace, která přinesla velký technologický
                        pokrok ve
                        znamení mikroprocesorů.
                        Mikroprocesor je číslicový integrovaný obvod s vysokou integrací, který vykonává počítačový program
                        uložený v
                        operační paměti počítače.
                        Díky tomu došlo ke zvýšení spolehlivosti, rychlosti a současně nastává velký ústup velkých sálových
                        počítačů
                        ve prospěch osobních PC.</p>
                    <p>Z uživatelského hlediska tak nastávají zlaté časy.
                        Dostáváme se totiž do éry operačního systému DOS a postupně vznikajících různých graficky
                        přívětivých
                        rozhraní.</p>
                    <p>IBM Personal Computer (IBM PC) byl představen 12. srpna 1981 s modelovým označením 5150.
                        Jde již v zásadě o stolní počítač, kdy skříň s hardwarem o rozměrech 51 × 41 × 14 cm doplňuje
                        standardní
                        televizor nebo RGB monitor.
                        A pochopitelně klávesnice. Jistě vás zajímá, jaké měl tento 40 let starý stroj parametry.
                        Konkrétně šlo o procesor Intel 8088 s frekvencí 4,77 MHz doplněný o 16 až 256 kB RAM. Pro ukládání
                        dat musela
                        stačit standardně dodávaná magnetická páska.
                        Nicméně si šlo přikoupit disketovou mechaniku pro 5,25" diskety s kapacitou 180 kB, což však bylo
                        finančně
                        mimořádně náročné.
                        Pro zobrazení grafického výstupu sloužil jak monitor, tak i klasická televize, která byla kvůli
                        vysokým cenám
                        monitorů daleko schůdnějším řešením.
                        Hlavní roli hrálo monochromatické zobrazení, protože drtivá většina práce nebo zábavy se odehrávala
                        v textovém
                        režimu.</p>
                    <img src="obrazky\ibm-pc-51501.jpg" alt="ibm-stolni-pocitac">
                    <p>Základní verze nového IBM PC 5150 se dala pořídit za zhruba 1 550 dolarů.
                        Při dokoupení dalších volitelných komponent, jako byla disketová mechanika nebo barevný CRT monitor,
                        však cena
                        vyletěla přes 3 000 dolarů,
                        což by byla vysoká cena za PC i v dnešní době.</p>

                    <!-- První Apple počítač -->
                    <h2 id="applePocitac">První Apple počítač</h2>
                    <p>Při pohledu do počítačové historie nesmíme v žádném případě vynechat ani Apple.
                        Prvním osobním počítačem, který začal psát velmi zajímavý příběh, byl stroj s jednoduchým označením
                        Apple I.
                        V roce 1976 bylo celé odvětví IT na samém počátku a nikdo nemohl tušit, co přinesou další roky.
                        Přesto se Steve Wozniak a Steve Jobs pustili do sestrojení vůbec prvního počítače, jehož myšlenka
                        vycházela z
                        vědecké kalkulačky.</p>
                    <img src="obrazky\prvni-pocita-apple-I.jpg" alt="apple-pocitac">
                    <p>Po představení v kalifornském Los Altos dokázali v ikonické garáži vyrobit 200 kusů.
                        Po hardwarové stránce nabízel Apple 1 operační paměť o velikosti 4 kB doplněnou mikroprocesorem MOS
                        Technology
                        6502 s frekvencí 1 MHz.
                        Počítač tvořila série obvodů, k nimž bylo třeba dokoupit napájecí zdroj a monitor, a navíc
                        doinstalovat
                        vlastní programy.
                        I přes cenu 500 dolarů byl zájem zcela mimořádný. Z dnešního pohledu jde o zcela raritní kousky,
                        takže Apple I
                        se snadno vydraží i za 500 000 dolarů.</p>

                    <!-- Výhledy do budoucnosti počítačů -->
                    <h2 id="budoucnost">Výhledy do budoucnosti počítačů</h2>
                    <p>Sami vidíte, že historie počítačů je velmi košaté téma a každá generace by si jistě zasloužila
                        detailnější
                        pohled.
                        Nicméně člověk je tvor zvídavý, a proto je dobré nežít jen minulostí, ale soustředit se i na
                        budoucnost.
                        Z historického hlediska jsme možná právě teď na prahu páté počítačové generace.
                        Zažíváme totiž rozmach umělé inteligence a stále více se spekuluje o kvantových počítačích.</p>

                </section>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>
<?php
require "./include/general.php";
require "./include/auth_guest.php";
require "./include/database.php";

$error = "";

// Pokud je formulář odeslaný jako POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $email = mysqli_real_escape_string($conn, $_POST["email"]);
    $password = mysqli_real_escape_string($conn, $_POST["password"]);

    $result = mysqli_query($conn, "SELECT * FROM `users` WHERE `email` = '$email'");

    if (mysqli_num_rows($result) === 0) {
        $error = "Nesprávné uživatelské údaje!";
    } else {
        $result = mysqli_fetch_assoc($result);

        if (password_verify($password, $result["password"])) {
            $_SESSION["id"] = $result["id"];
            $_SESSION["email"] = $result["email"];
            $_SESSION["firstname"] = $result["firstname"];
            $_SESSION["lastname"] = $result["lastname"];
            $_SESSION["phone"] = $result["phone"];
            $_SESSION["admin"] = (int)$result['is_admin'] === 1;

            header("Location: ./profile.php");
            die();
        } else {
            $error = "Nesprávné uživatelské údaje!";
        }
    }
}
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.html" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Přihlášení</h1>
                    </header>

                    <hr class="major" />
                </section>

                <form method="post" action="#">
                    <div class="row gtr-uniform">
                        <div class="col-6 col-12-xsmall">
                            <input type="text" name="email" id="email" placeholder="Email" /><br>
                            <input type="password" name="password" id="pass" placeholder="Heslo" />
                        </div>

                        <div class="col-12">
                            <button type="submit" class="button primary">
                                Odeslat
                            </button>
                        </div>

                        <p id="message" style="color: red;"><?php echo $error ?></p>
                    </div>
                </form>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>
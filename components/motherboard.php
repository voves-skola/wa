<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Základní desky</h1>
                    </header>

                    <hr class="major" />
                    <p>Jestliže procesor můžeme označit za mozek počítače, pak základní deska je něco jako jeho srdce.
                        Tento zásadní komponent je osazován hardwarem, mezi který patří zmíněný procesor, grafická karta, paměť, síťová
                        karta a podobně.</p>
                    <p>Jádrem každé základní desky je tzv. chipset neboli čipová sada (např. Intel Z170, Intel x99,…). K
                        nejpoužívanějším formátům dnes patří ATX, Micro ATX a Mini-ITX.</p>
                    <p>U základní desky je také velmi důležitá zvolená patice neboli socket.
                        Nejčastěji se jedná o 1151 socket, 1150 socket, ale variant je celá řada.
                        Další – a pro většinu běžných uživatelů možná nejdůležitější – parametr je kompatibilita s procesorem. Možnosti
                        jsou v zásadě dvě – AMD a Intel.</p>
                    <p>Výběr základní desky také záleží na použitých řadičích (M.2, USB 3.0, USB 3.1 apod.),
                        slotu pro paměť (DDR4 DIMM, DDR3 DIMM,…) a také samozřejmě výrobci – mezi ty nejvýznamnější dnes patří ASRock,
                        ASUS, GIGABYTE a MSI.</p>
                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>